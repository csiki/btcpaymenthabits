import urllib2
import json
import time


def req_transactions(address, wait=True):
    try:
        r = urllib2.urlopen("https://bitcoin.toshi.io/api/v0/addresses/%s/transactions" % address)
        txs = r.read()
        r.close()
    except urllib2.HTTPError:
        print "Error with address: %s" % address
        return None
    if wait:
        wait_till_next_req(0.3)

    try:
        txs_obj = json.loads(txs)
    except ValueError:
        print "JSON load error with address %s:" % address
        with open('err.txt', 'wt') as f:
            f.write(txs)
        return None

    return txs_obj


def req_address_data(address, wait=True):
    try:
        r = urllib2.urlopen("https://blockchain.info/address/%s?format=json" % address)
        addr = r.read()
        r.close()
    except urllib2.HTTPError:
        print "Error with address: %s" % address
        return None
    if wait:
        wait_till_next_req(1)
    return json.loads(addr)


def req_addresses(wait=True):
    r = urllib2.urlopen("https://blockchain.info/unconfirmed-transactions?format=json")
    latest_txs_json = r.read()
    r.close()
    if wait:
        wait_till_next_req(1)

    latest_txs = json.loads(latest_txs_json)
    addrs = []
    for tx in latest_txs['txs']:
        for addr in tx['out']:
            if 'addr' in addr:
                addrs.append(addr['addr'])

    return addrs


def wait_till_next_req(wait_time=0.6):
    time.sleep(wait_time)

