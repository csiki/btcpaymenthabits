import blockchain_api
import os.path
import pickle
import time
import datetime
import matplotlib.pyplot as plt
import numpy as np
import powerlaw
from scipy.stats import binned_statistic

# new address acquisition
# gather addresses and save them to addrs.pydat
def update_address_list(file_path='addrs.pydat', n_updates=100):
    # load
    addrs = set()
    if os.path.isfile(file_path):
        with open(file_path, 'rb') as f:
            addrs = pickle.load(f)
    # update
    for u in xrange(n_updates):
        new_addrs = blockchain_api.req_addresses()
        for new_addr in new_addrs:
            addrs.add(new_addr)
    # save
    with open(file_path, 'wb') as f:
        pickle.dump(addrs, f)

    return len(addrs)


# account data acquisition (with transaction times)
# gather account data and save them to accdata.pydat
def update_account_data(acc_data_path='accdata.pydat', addr_list_path='addrs.pydat', processed_addr_path='proc_addrs.pydat', save_after_every=100):
    # accound data
    acc_data = []
    if os.path.isfile(acc_data_path):
        with open(acc_data_path, 'rb') as f:
            acc_data = pickle.load(f)
    # processed addresses
    proc_addrs = set()
    if os.path.isfile(processed_addr_path):
        with open(processed_addr_path, 'rb') as f:
            proc_addrs = pickle.load(f)
    # addresses
    addrs = None
    with open(addr_list_path, 'rb') as f:
        addrs = pickle.load(f)
    # iterate through addresses
    addr_id = len(proc_addrs)
    for addr in addrs:
        if addr not in proc_addrs:
            adat = blockchain_api.req_address_data(addr)
            if adat is not None:
                acc_data.append(adat)
                proc_addrs.add(addr)
                addr_id += 1
                if addr_id % save_after_every == 0:
                    print "Addresses processed so far: %d" % addr_id
                    with open(acc_data_path, 'wb') as f:  # save account data
                        pickle.dump(acc_data, f)
                    with open(processed_addr_path, 'wb') as f:  # save processed addresses
                        pickle.dump(proc_addrs, f)


def extract_time_info(addr_list_path='addrs.pydat', time_data_path='time_info.pydat', save_after_every=100):
    # addresses
    addrs = None
    with open(addr_list_path, 'rb') as f:
        addrs = pickle.load(f)
    # transactions
    time_data = dict()
    if os.path.exists(time_data_path):
        with open(time_data_path, 'rb') as f:
            time_data = pickle.load(f)
    addr_count = 0
    for addr in addrs:
        if addr not in time_data:
            txs = blockchain_api.req_transactions(addr)
            if txs is not None:
                try:
                    time_data[addr] = [(time.mktime(datetime.datetime.strptime(tx['block_time'], "%Y-%m-%dT%H:%M:%SZ").timetuple()),
                                        tx['amount']) for tx in txs['transactions']]
                except KeyError:
                    print 'Error: probably no block_time info on address %s' % addr
                    continue
                addr_count += 1
                if addr_count % save_after_every == 0:
                    print "Addresses processed so far: %d" % addr_count
                    with open(time_data_path, 'wb') as f:
                        pickle.dump(time_data, f)
    print "Finished processing %d addresses" % addr_count
    with open(time_data_path, 'wb') as f:
        pickle.dump(time_data, f)


def extract_inter_payment_times(time_data_path='time_info.pydat', min_tx_num=60):
    # load transactions
    with open(time_data_path, 'rb') as f:
        time_info = pickle.load(f)

    # iterate through addresses & save inter payment times
    inter_payment_times = []
    payment_amounts = []
    for addr, txs in time_info.items():
        if len(txs) > min_tx_num:
            inter_payment_times.append([txs[tx_index-1][0] - txs[tx_index][0] for tx_index in xrange(len(txs)-1, 0, -1)])
            payment_amounts.append([txs[tx_index-1][1] for tx_index in xrange(len(txs)-1, 0, -1)])

    return np.array([tx for txs in inter_payment_times for tx in txs]), np.array([tx for txs in payment_amounts for tx in txs])

def plot_binned_shit(x, y, bins=None):
    y = y[x > 0]
    x = x[x > 0]
    if bins is None:
        bins = np.logspace(np.log10(min(x)), np.log10(max(x)), 10)
    fig = plt.figure()
    ax = fig.add_subplot(1,1,1)
    ax.set_title('Payment amounts by delay times')
    ax.set_xlabel('Delay times')
    ax.set_ylabel('Payment amount (Satoshi)')
    ax.loglog()
    bin_means, bin_edges, binnumber = binned_statistic(x, y, bins=bins)
    bin_means[np.isnan(bin_means)] = 0
    ax.vlines(bin_edges, 0, bin_means)
    ax.vlines(bin_edges[:-1] + bin_edges[1:] - bin_edges[:-1], 0, bin_means)
    ax.hlines(bin_means, bin_edges[:-1], bin_edges[1:])

# main
if __name__ == '__main__':
    # n_addrs_needed = 200000
    # n_addrs = 0
    # while n_addrs < n_addrs_needed:
    #     n_addrs = update_address_list()
    #     print 'Number of addresses so far: %d' % n_addrs

    # update_account_data()
    # extract_time_info()

    # gain data
    inter_payment_times_orig, payment_amounts_orig = extract_inter_payment_times()
    greater_than_600_not_nan = inter_payment_times_orig > 600 & ~np.isnan(inter_payment_times_orig)
    inter_payment_times = inter_payment_times_orig[greater_than_600_not_nan]  # filter out less than 10-minute delays & nans
    payment_amounts = payment_amounts_orig[greater_than_600_not_nan]
    inter_payment_times = inter_payment_times.astype(int)

    # powerlaw_res = powerlaw.Fit(inter_payment_times[inter_payment_times > 1000000])
    # print powerlaw_res.alpha, powerlaw_res.xmin  # all: 2.50461902497 1617375.0; 2.50461902497 1617375.0; 2.50461902497 1617375.0
    # exit(1)

    # plotting
    # plt.hist(inter_payment_times, bins=np.logspace(np.log10(np.min(inter_payment_times)), np.log10(np.max(inter_payment_times)), 50))
    # plot ccdf
    # sorted_vals = np.sort(np.unique(inter_payment_times))
    # ccdf = np.zeros(len(sorted_vals))
    # n = float(len(inter_payment_times))
    # for i, val in enumerate(sorted_vals):
    #     ccdf[i] = np.sum(inter_payment_times >= val) / n
    with open('ccdf.pydat', 'rb') as f:
        sorted_vals, ccdf = pickle.load(f)
    plt.plot(sorted_vals, ccdf, "-")
    # save ccdf data
    # with open('ccdf.pydat', 'wb') as f:
    #     pickle.dump((sorted_vals, ccdf), f)
    # plot fitting
    power_law_x = np.linspace(10000, 100000000, num=1000)
    power_law_y = np.power(power_law_x, -2.50461902497 + 1) * 1617375
    plt.plot(power_law_x, power_law_y)
    plt.loglog()
    plt.legend(['ccdf', 'fit'])
    plt.xlabel('Delay times (sec)')
    plt.ylabel('1-CDF')
    plt.title('Complementary cumulative distribution of Bitcoin transaction delay times')
    np.savetxt('inter_payment_times_10min.txt', inter_payment_times, delimiter='\n', fmt='%d')
    # plot amount vs
    plt.figure()
    plt.scatter(inter_payment_times, payment_amounts)
    plt.xlabel('Delay times (sec)')
    plt.ylabel('Payment amount (Satoshi)')
    plt.title('Payment amounts by delay times')
    plt.semilogx()

    # plot_binned_shit(inter_payment_times, payment_amounts)

    plt.show()

    # counter = 0
    # for txs in inter_payment_times:
    #     # plt.scatter(range(len(txs)), txs)
    #     plt.plot(range(len(txs)), txs)
    #     plt.title('Payment delays for account #%d' % counter)
    #     plt.xlabel('Payment event number')
    #     plt.ylabel('Delay time')
    #     plt.show()
    #     counter += 1
    # plt.loglog()
    # plt.show()

    # what's the _probability_ that an email is sent after t time


